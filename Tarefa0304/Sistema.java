import java.util.ArrayList;

interface Sistema{

    public void rodar(){
        Sistema sistema = new Sistema();
        
        Receita receita1 = new Receita();
        receita1.setReceita("Paracetamol 1mg 20 comprimidos");
        OCR.ler(receita1);

        Receita receita2 = new Receita();
        receita2.setReceita("Desvenlafaxina 50mg 300 comprimidos");
        OCR.ler(receita2);


        Receita receita3 = new Receita();
        receita3.setReceita("Topiramato 10mg 30 comprimidos");
        OCR.ler(receita3);


        OCR.imprimirReceita();
    }

    public static void main (String args[]){
        new Sistema().rodar();
    }
}
