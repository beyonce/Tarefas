public class SistemaDeVendas {
    
    private MaquinaDeIngressos maquina;

    public void rodar(){

        //Caso de uso [Público]: Comprar ingresso
        maquina = new MaquinaDeIngressos("Halloween", 14.5, 20);

        //Caso de uso[Gerência]: Consultar saldo
        maquina.comprarIngresso(14.5);
        //Fim Comprar ingresso

        //Caso de uso[Gerência]: Consultar saldo
        maquina.comprarIngresso(14.5);
        //Fim Comprar ingresso

        //Caso de uso[Gerência]: Consultar saldo
        maquina.consultarSaldo();
        
        maquina.imprimirRelatorio();

    }

    public static void main(String[] args) {
        new SistemaDeVendas().rodar();
    }
}
