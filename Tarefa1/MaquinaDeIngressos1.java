public class MaquinaDeIngressos {
    //Valor do ingresso vendido
    private double valor;
    //Total de ingressos vendidos
    private int totalIngressosVendidos;
    //Valor total das vendas
    private double saldo;
    //total de ingressos á venda
    private int totalIngressos;
    //Nome do Evento
    private String nomeDoEvento;
    //Troco
    private double trocoIngresso;

    public trocoIngresso(double trocoIngresso){
        trocoIngresso = (this.valor - valor);
    }

    public MaquinaDeIngressos(double valor, int totalIngressos){
        this.valor = valor;
        this.totalIngressos = totalIngressos;
        this.nomeDoEvento = nomeDoEvento;
    }

    public void comprarIngresso(double valor){
        //Se o valor recebido for suficiente para comprar um ingresso, venda o ingresso.
        if(valor >= this.valor){
            saldo = saldo + this.valor;
            totalIngressos--;
            totalIngressosVendidos++;
            //ou totalIngressosVendidos = totalIngressosVendidos + 1;
            this.imprimirIngresso();
        }
        if (valor < this.valor) {
            System.out.println("Valor insuficiente para concluir transação.");
        }
    }

    public void consultarSaldo(){
        System.out.println("Saldo: " +saldo);
    }

    public void imprimirRelatorio(){
        System.out.println();
        System.out.println("------------------");
        System.out.println("Relatório de vendas");
        System.out.println("------------------");
        System.out.println("Evento: " +nomeDoEvento);
        System.out.println("Total de ingressos vendidos: " +totalIngressosVendidos);
        System.out.println("Valor dos ingressos: " +this.valor);
        System.out.println("Saldo total: " +this.saldo);               
    }

    private void imprimirIngresso(){
        System.out.println();
        System.out.println("Ingresso para " +nomeDoEvento);
        System.out.println("---------");
        System.out.println("Valor: " +this.valor);
        System.out.println("Troco: " +trocoIngresso);
    }
}