public class IngressoVip extends Ingresso{
    public IngressoVip(double valor){
        super(valor+10);
    }
    public String toString(){
        return "***VIP***\n"+
               super.toString();
    }
}