public class MaquinaDeVendas{
    private double valorDoIngresso;
    private Impressora impressora;
    
    public MaquinaDeVendas(double ValorDoIngresso){
        this.valorDoIngresso = valorDoIngresso;
        this.impressora = new Impressora();
    }
    
    public Ingresso venderIngresso(double valor){
        if(valor > valorDoIngresso){
            IngressoVip ingresso = new IngressoVip(valor);
            
            impressora.imprimir(ingresso);
            
            return ingresso;
        }else if(valor == valorDoIngresso){
            Ingresso ingresso = new Ingresso(valor);
            
            impressora.imprimir(ingresso);
            
            return ingresso;
        }else{
            return null;
        }
        
        
    }
    
}