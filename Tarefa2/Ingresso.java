public class Ingresso{
    private double valor;
    
    public Ingresso(double valor){
        this.valor = valor;
    }
    
    public String toString(){
        return "Ingresso\n"+
               "--------\n"+
               "Valor: R$" +valor;
    }
}