# Material de aula do primeiro dia de curso

### Tópicos

- Verificação de conteúdos estudados
- Como criar um repositório
- Como instalar o BlueJ

1. Verificação de conteúdos estudados
1. Como criar um repositório
1. Como instalar o BlueJ

### Exemplo de programa em Java

Este é um exemplo de 'Hello World' escrito em Java.

```
class Hello{
    public static void main(String arg[]){
        System.out.println("Hello World");
    }
}
```

[volar para o inicio](xxx-xxx.md)