public class Livro {
    private String titulo;
    private String genero;
    private int isbn;

    public String getTitulo(){
        return titulo;
    }

    public String getGenero(){
        return genero;
    }

    public int getIsbn(){
        return isbn;
    }

    public void setTitulo(String titulo){
        this.titulo = titulo;
    }

    public void setGenero(String genero){
        this.genero = genero;
    }

    public void setIsbn(int isbn){
        this.isbn = isbn;
    }
}
