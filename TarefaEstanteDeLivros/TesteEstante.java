import java.util.ArrayList;
import java.util.HashMap;

public class TesteEstante {
    HashMap<String, ArrayList<Livro>> livrosPorGenero;

    public TesteEstante(){
        livrosPorGenero = new HashMap<String, ArrayList<Livro>>();
        livrosPorGenero.put("Fantasia", new ArrayList<Livro>());
        livrosPorGenero.put("Ficção", new ArrayList<Livro>());
    }

    public void colocar(String genero, Livro livro){
        this.livrosPorGenero.get(genero).add(livro);
    }

    public void imprimirRelatorio(String genero){
            ArrayList<Livro> livros = livrosPorGenero.get(genero);
            //Cabeçalho
            System.out.println("Relatório " +genero);
            System.out.println("----------------");
    
            //Listagem de livros
            for(int i = 0; i<livros.size(); i++){
                Livro livro = livros.get(i);
                System.out.println("Título: " +livro.getTitulo());
                System.out.println("Gênero: " +livro.getGenero());
                System.out.println("Isbn: " +livro.getIsbn());
                System.out.println("------------------");
            }
    
            System.out.println("Total: " +livros.size());
        }

    public void rodar(){
        TesteEstante estante = new TesteEstante();

        Livro livro1 = new Livro();
        livro1.setTitulo("O Sociedade Do Anel");
        livro1.setGenero("Fantasia");
        livro1.setIsbn(859508475);
        estante.colocar("Fantasia", livro1);

        Livro livro2 = new Livro();
        livro2.setTitulo("As Duas Torres");
        livro2.setGenero("Fantasia");
        livro2.setIsbn(859508476);
        estante.colocar("Fantasia", livro2);

        Livro livro3 = new Livro();
        livro3.setTitulo("O Retorno Do Rei");
        livro3.setGenero("Fantasia");
        livro3.setIsbn(859508477);
        estante.colocar("Fantasia", livro3);

        Livro livro4 = new Livro();
        livro4.setTitulo("Frankenstein");
        livro4.setGenero("Ficção");
        livro4.setIsbn(859508477);
        estante.colocar("Ficção", livro4);

        Livro livro5 = new Livro();
        livro5.setTitulo("A Guerra dos Mundos");
        livro5.setGenero("Ficção");
        livro5.setIsbn(859508477);
        estante.colocar("Ficção", livro5);
        
        estante.imprimirRelatorio("Fantasia");
        
        estante.imprimirRelatorio("Ficção");
    }

    public static void main(String args[]){
        new TesteEstante().rodar();
    }
}
